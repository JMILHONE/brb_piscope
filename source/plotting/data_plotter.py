from __future__ import division, print_function
from distutils.util import strtobool
from .resample import DataDisplayDownsampler
import numpy as np
from ..gui.helpers import global_lcm
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib import rcParams
rcParams['font.size'] = 16

def plot_all_data(axs, locs, data, downsampling=10000):
    """
    Plot all data on the set axes.

    Parameters
    ----------
    axs : list[list[plt.Axes]]
        A list of lists to index axes by row and column for plotting.
    locs : list[str]
        String of two ints that define which axis to plot on.
    data : dict[str, list[Data]]
        Dictionary of data to plot and where.

    Returns
    -------
    down_samplers : list[DataDisplayDownsampler]
        All downsampler objects created while plotting.
    """
    down_samplers = []
    for pos in locs:
        i, j = (int(x) for x in pos)
        down_samplers += [plot(axs[j][i], locs[pos], data[pos], downsampling=downsampling)]

    return down_samplers


def plot(ax, info_dict, data, downsampling=10000):
    """
    Plot the Data objects from `data` on the `ax` Axes with the set downsampling.

    Parameters
    ----------
    ax : plt.Axes
    info_dict : dict
        Dictionary to use when setting axes options.
    data : list[Data]
    downsampling : int, default=10000
        Maximum number of points to plot per line.

    Returns
    -------
    down_sampler : DataDisplayDownsampler
    """
    if data is None:
        return
    info_keys = info_dict.keys()

    actual_data = []
    xstart = np.inf
    xend = -np.inf
    for d in data:
        #if len(d.data) > 1:
        if d:  # Data class is now Truthy
            actual_data += [d]

            # Set xstart and xend to the earliest and latest x values.
            if d.time[0] < xstart:
                xstart = d.time[0]

            if d.time[-1] > xend:
                xend = d.time[-1]
    # Create the downsampler object with all non-empty data.
    down_sampler = DataDisplayDownsampler(actual_data, xend - xstart, ax, max_points=downsampling)

    try:
        noresample = info_dict['noresample']
    except KeyError:
        noresample = False

    for d in actual_data:
        if not noresample:
            # Downsample the data and add the plotted line to the downsampler.
            x, y = down_sampler.downsample(d, xstart, xend)
            line, = ax.plot(x, y, label=d.name, color=d.color, lw=1)
            down_sampler.lines.append(line)
        else:
            # print(d.name, "not resampling!")
            x, y = d.time, d.data
            line, = ax.plot(x, y, label=d.name, color=d.color, lw=1)

    lg = None
    if 'legend' in info_keys and strtobool(info_dict['legend']):
        lg = ax.legend()

    if 'xlabel' in info_keys:
        ax.set_xlabel(info_dict['xlabel'])

    if 'ylabel' in info_keys:
        ax.set_ylabel(info_dict['ylabel'])

    if lg:
        lg.set_draggable(True)

    if 'xlim' in info_keys:
        xlim = info_dict['xlim']
        xlim = [float(x) for x in xlim]
        ax.set_xlim(xlim)

    if 'ylim' in info_keys:
        ylim = info_dict['ylim']
        ylim = [float(y) for y in ylim]
        ax.set_ylim(ylim)

    if len(down_sampler.lines) > 0:
        # Set the axes to update differently when the x limits change.
        ax.set_autoscale_on(False)
        ax.callbacks.connect('xlim_changed', down_sampler.update)

    return down_sampler


def create_figure(column_setup):
    """
    Construct the figure, axes, and axes positioning to be used for plotting.

    Parameters
    ----------
    column_setup : list[int]
        List of number of rows per column.

    Returns
    -------
    figure : plt.Figure
    axs : list[list[plt.Axes]]
    grid_spec : GridSpec
        The grid layout to use for subplots while plotting.
    """
    figure = plt.figure(0)

    axs = []
    valid_rows_per_column = [x for x in column_setup if x > 0]
    lcm = global_lcm(valid_rows_per_column)
    num_cols = len(valid_rows_per_column)
    grid_spec = GridSpec(lcm, num_cols)
    # grid_spec.update(wspace=0.15, hspace=0.15)

    for i, num_rows in enumerate(valid_rows_per_column):
        factor = lcm // num_rows
        axes = []
        for j in range(num_rows):
            # print(lcm, num_cols, factor*j, i, factor)
            # ax = plt.subplot2grid((lcm, num_cols), (factor * j, i), rowspan=factor, fig=figure)
            # Add the axes to the figure covering `factor` rows.
            ax = figure.add_subplot(grid_spec[factor * j:factor * (j + 1), i])
            axes.append(ax)
        axs.append(axes)

    # grid_spec.tight_layout(figure)
    return figure, axs, grid_spec

