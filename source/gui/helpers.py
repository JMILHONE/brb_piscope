from __future__ import division, print_function


# TODO: Change this to using numpy.lcm
def find_lcm(num1, num2):
    """
    Get the least common multiple between two integers.

    Parameters
    ----------
    num1, num2 : int

    Returns
    -------
    lcm : int
    """
    if (num1 > num2):
        numerator = num1
        denominator= num2
    else:
        numerator = num2
        denominator= num1

    remainder = numerator % denominator
    while remainder != 0:
        numerator = denominator
        denominator = remainder
        remainder= numerator % denominator

    gcd = denominator
    lcm = int(int(num1 * num2) / int(gcd))
    return lcm


def global_lcm(numbers):
    if len(numbers) < 2:
        return numbers[0]
    elif len(numbers) == 2:
        return find_lcm(numbers[0], numbers[1])
    else:
        num1 = numbers[0]
        num2 = numbers[1]
        lcm = find_lcm(num1, num2)

        for i in range(2, len(numbers)):
            lcm = find_lcm(lcm, numbers[i])

        return lcm


