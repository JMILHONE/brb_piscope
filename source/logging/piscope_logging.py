from __future__ import print_function, division
import logging
from functools import wraps
import time

def create_logger(name='pi-scope-logger', filename='piscope-debug.log', useNull=False):
    """
    Create a logger object for logging piscope status.

    Parameters
    ----------
    name : str
        Name of the Logger object.
    filename : str
        Filename to write log output to if `useNull` is `False`.
    useNull : bool
        `True` if logging should be disabled and a `NullHandler` will be used.

    Returns
    -------
    logging.Logger
    """
    logger = logging.getLogger(name)

    if useNull:
        handler = logging.NullHandler()
    else:
        handler = logging.FileHandler(filename=filename, mode='w')
        logger.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%m/%d/%Y %I:%M:%S %p')

        handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


def log(logger):
    """
    Get a decorator to log the calling of a function.

    Parameters
    ----------
    logger: logging.Logger
        The logging object.

    Returns
    -------
    decorator : func(func) -> func
        A function decorator that logs calling of the function.

    Examples
    --------
    >>> logger = create_logger('test-logger', 'tmp.txt')
    >>> @log(logger)
    ... def f():
    ...     return True
    >>> f()

    There should now be a line of text in `tmp.txt` that logs the calling of `f`.
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            logger.debug('Calling function %s' % func.__name__)
            try:
                return func(*args, **kwargs)
            except:
                logger.exception('Error in %s' % func.__name__)
                raise
        return wrapper

    return decorator


def time_log(logger):
    """
    Get a decorator to log the calling of a function along with the time taken to complete.

    Parameters
    ----------
    logger: logging.Logger
        The logging object.

    Returns
    -------
    decorator : func(func) -> func
        A function decorator that logs calling of the function.

    Examples
    --------
    >>> logger = create_logger('test-logger', 'tmp.txt')
    >>> @log(logger)
    ... def f():
    ...     return True
    >>> f()

    There should now be a line of text in `tmp.txt` that logs the calling of `f` and the time taken to complete.
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                t0 = time.time()
                res = func(*args, **kwargs)
                t1 = time.time()
                logger.debug('Calling function %s, completed in %f seconds' % (func.__name__, t1-t0))
                return res
            except:
                logger.exception('Error in %s' % func.__name__)
                raise
        return wrapper
    return decorator

