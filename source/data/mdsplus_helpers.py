from __future__ import division, print_function
import MDSplus as mds
from .data import Data
from ..logging.piscope_logging import log, time_log
import logging
from functools import lru_cache
import concurrent.futures
from queue import SimpleQueue, Empty
from configobj import Section
import numpy as np

logger = logging.getLogger('pi-scope-logger')

ignore_items = ['legend', 'xlabel', 'ylabel', 'xlim', 'ylim', 'color', 'noresample', 'xshare']

def retrieve_all_data(server, tree, shot_number, config, progress_signal=None):
    """
    Get all data from MDSplus defined by the config into a dictionary.

    Parameters
    ----------
    server, tree : str
    shot_number : int
        Parameters for connecting to the MDSplus tree.
    config : ConfigObj
    progress_signal : 
        TODO: Figure out what this is supposed to do.

    Returns
    -------
    data : dict(str, list[Data])
    """
    signals_to_grab = []
    data = dict()

    # Prep the data dictionary with empty lists for each subplots and grab all of the names of signals.
    for subplot_name, subplot in config.items():
        data[subplot_name] = list()
        for signal_name in subplot:
            if signal_name not in ignore_items:
                signals_to_grab.append((subplot_name, signal_name))

    n_items = len(signals_to_grab)
    n = 0

    num_workers = 1
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        # Create a pool of connections equal to the number of workers that we can pull connections from instead of creating a new connection each time.
        connection_queue = SimpleQueue()
        for _ in range(num_workers):
            conn = mds.Connection(server)
            conn.openTree(tree, shot_number)
            connection_queue.put(conn)

        future_to_signal = {executor.submit(retrieve_signal, connection_queue, config[subplot_name][signal_name], signal_name): (subplot_name, signal_name)
                            for (subplot_name, signal_name) in signals_to_grab}

        for future in concurrent.futures.as_completed(future_to_signal):
            subplot, name = future_to_signal[future]
            data[subplot].append(future.result())
            n += 1
            if progress_signal:
                progress_signal.emit(int(n / n_items * 100.0))

        # Clean up the connection queue.
        while not connection_queue.empty():
            try:
                conn = connection_queue.get(timeout=0.1)
            except Empty:
                break
            conn.disconnect()

    return data


@log(logger)
def get_current_shot(server, tree):
    """
    Get the shot number of the most recent shot.

    Parameters
    ----------
    server, tree : str

    Returns
    -------
    int or None
        Most recent shot number or None if there was an error while getting shot.
    """
    try:
        con = mds.Connection(server)
        con.openTree(tree, 0)
        current_shot = int(con.get("$SHOT"))
        return current_shot
    except mds.mdsExceptions.TreeNOCURRENT as e:
        logger.warning('TreeNOCURRENT in get_current_shot')
        return
    except (mds.MdsIpException, mds.TreeFOPENR, mds.TdiMISS_ARG) as e:
        logger.warning('Random MDSplus error in get_current_shot')
        return
    except ValueError as e:
        logger.warning('ValueError in get_current_shot')
        return


@log(logger)
def check_data_dictionary(data_dict):
    for d in data_dict:
        for item in data_dict[d]:
            if item:  # Class Data is now Truthy
                return True
    # If you make it to here, that means all items were None
    return False


@log(logger)
def check_open_tree(shot_number, server, tree):
    """
    Test if a connection can be made to the MDSplus tree.

    Parameters
    ----------
    shot_number : int
    server : str
    tree : str

    Returns
    -------
    bool
    """
    try:
        con = mds.Connection(server)
        con.openTree(tree, shot_number)
        return True
    except (mds.MdsIpException, mds.TreeFOPENR, mds.TdiMISS_ARG) as e:
        logger.warning('Error opening shot %d' % shot_number)
        return False


@log(logger)
def retrieve_signal(connection_queue, signal_info, signal_name):
    """
    Extract signal parameters from `signal_info` and retrieve data from the MDSplus tree.

    Parameters
    ----------
    connection_queue : SimpleQueue
        Queue of connection objects to use when getting data.
    signal_info : dict[str, str]
    signal_name : str

    Returns
    -------
    Data or None
        Data retrieved from MDSplus or None if there was an error.
    """
    xstring = signal_info['x']
    ystring = signal_info['y']
    color = signal_info['color']
    if isinstance(ystring, list):
        ystring = ','.join(ystring)
    if isinstance(xstring, list):
        xstring = tuple(xstring)

    logger.debug("Retreiving signal: xstring: {x}, ystring: {y}, color: {c}".format(x=xstring, y=ystring, c=color))
    # Get a connection. Since there are the same number of workers as connection objects there should always be a valid connection.
    connection = connection_queue.get_nowait()
    #print(type(shot_number), type(server), type(tree), type(xstring), type(ystring), type(signal_name), type(color), ystring)
    data = _retrieve_signal(connection, xstring, ystring, signal_name, color)
    
    # Put the connection back in the queue.
    connection_queue.put(connection)

    return data
    # return loc_name, signal_name, data


@lru_cache(maxsize=512)
def _retrieve_signal(connection, xstring, ystring, name, color):
    """
    Retrieve the signal by either creating a constant Data object or use data from MDSplus.

    Parameters
    ----------
    connection : mds.Connection
    xstring : str
        String to get x data for signal from MDSplus.
    ystring : str
        String to get y data for signal from MDSplus.
    name : str
        Name of signal.
    color : str
        Color of signal to plot.

    Returns
    -------
    Data or None
        Returns the data gotten from the MDSplus tree or None if there was an error along the way.
    """
    # Check if the signal is a constant value. If so then return a Data object which has a constant y value in the specified x tuple.

    # Change the y string to a float.
    try:
        y_constant = float(ystring)
        is_constant = True
    except ValueError:
        is_constant = False

    if is_constant:
        cant_tuple_warning = lambda description: logger.warning("Error parsing {name}: 'x' string {xstring} could not be changed to a tuple with two floats: {desc}".format(name=name, xstring=xstring, desc=description))

        if not isinstance(xstring, tuple) or len(xstring) != 2:
            cant_tuple_warning("Not a tuple or not of length 2.")
            return

        # The xstring is already a list of two strings so we just need to change them to floats.
        cleaned_string = xstring
        cleaned_strings = [s.strip() for s in xstring]
        if cleaned_string[0] == '(' and cleaned_string[-1] == '(':
            cleaned_string = cleaned_string[1:-1]

        lower_x_lim = cleaned_string[0]
        upper_x_lim = cleaned_string[1]
        try:
            lower_x_lim = float(lower_x_lim)
            upper_x_lim = float(upper_x_lim)
        except ValueError:
            cant_tuple_warning("Could not change x limits to floats.")
            return

        # Return a data object.
        num_points = 100 # This value really shouldn't matter but we'll set it nice and large.
        return Data(name, np.linspace(lower_x_lim, upper_x_lim, num=100), y_constant * np.ones(num_points), color)
    else:
        # If not a constant then pull it from the MDSplus database.
        try:
            logger.debug("Retrieving data for %s" % name)
            data = retrieve_data(connection, xstring, ystring, name, color)
            return data
        except mds.MdsIpException as e:
            logger.warning('MDSplus IP exception occured while retrieving {}.'.format(name))
            return
        except mds.TreeFOPENR as e:
            logger.warning('MDSplus TreeFOPENR exception occured while retrieving {}.'.format(name))
            return
        except mds.TdiMISS_ARG as e:
            logger.warning('MDSplus TdiMISS_ARG exception occured while retrieving {}.'.format(name))
            return


def empty_lru_cache():
    """Empty the `_retrieve_signal` cache."""
    size_before_empty = _retrieve_signal.cache_info().currsize
    _retrieve_signal.cache_clear()
    logger.debug("Cache had %d items before clearing" % size_before_empty)


def log_lru_cache():
    """Log the success rate of the `_retrieve_signal` cache."""
    cache_info = _retrieve_signal.cache_info()
    hits = cache_info.hits
    misses = cache_info.misses
    maxsize = cache_info.maxsize
    currsize = cache_info.currsize

    logger.debug(
        "Cache has %d hits, %d misses with %d items out of the maximum %d items" % (hits, misses, currsize, maxsize)
    )


@time_log(logger)
def retrieve_data(connection, xstr, ystr, name, color):
    """
    Get data from MDSplus and return a Data object for plotting.

    Parameters
    ----------
    connection : mds.Connection
    xstr : str
        String to get x data from MDSplus.
    ystr : str
        String to get y data from MDSplus.
    name : str
        Name of data signal.
    color : str
        Color of signal to plot.

    Returns
    -------
    Data or None
        Data gotten from MDSplus or None if there was an error.
    """
    if "\n" in ystr:
        ystring = ystr.splitlines()
        ystring = " ".join(ystring)
    else:
        ystring = ystr

    if "\n" in xstr:
        xstring = xstr.splitlines()
        xstring = " ".join(xstring)
    else:
        xstring = xstr

    try:
        # Retrieve the x and y data.
        data = connection.get(ystring)
        t = connection.get(xstring)

        # Sometimes None can be returned without errors. If this is the case, return None.
        if data is None or t is None:
            logger.warning("Got 'None' from MDSplus during call while retrieving data for {}".format(name))
            return

        data = data.data()
        t = t.data()
        return Data(name, t, data, color)

    except mds.MdsIpException:
        logger.warning('MdsIPException occurred in retrieve_data for {}.'.format(name))
        return
    except mds.TreeNODATA:
        logger.warning('TreeNODATA occurred in retrieve_data for {}.'.format(name))
        return
    except mds.TreeNNF:
        logger.warning('TreeNNF occurred in retrieve_data for {}.'.format(name))
        return 
    except KeyError:
        logger.warning('KeyError occured in retrieve_data for {}.'.format(name))
        return

